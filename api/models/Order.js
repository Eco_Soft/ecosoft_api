/**
* Order.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    category : { type: 'string' },

    username : { type: 'string' },

    phone : { type: 'string' },

    coords : { type: 'string' },

    radius : { type: 'string' },

    image : { type: 'string' }
  }
};

